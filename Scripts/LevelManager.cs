using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : Singleton<LevelManager>
{

    private int levelIndex = 0;
    private int tempIndex = 0;

    public void NextLevel()
    {
        CameraMovement.currentEnemyIndex = 0;
        //levelIndex++;
        //PlayerPrefs.SetInt("LevelIndex", levelIndex);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex );
    }
}
