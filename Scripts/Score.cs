using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    CameraMovement camera;

    public GameObject scoreLine;
    public GameObject hightScoreLine;

    public Text scoreText;

    float score = 0;
    [HideInInspector]
    public bool stopUpdate = false;

    private void Start()
    {
        hightScoreLine.transform.position = new Vector3(PlayerPrefs.GetFloat("HightScore" , -15) * -1, 0.5f, 8f);
        camera = FindObjectOfType<CameraMovement>();
        scoreLine.SetActive(false);
    }
    private void LateUpdate()
    {
        if (!stopUpdate)
        {
            Vector3 scoreLineXTransform = new Vector3(camera.GetEnemy().transform.position.x, 0.5f, 8f);
            
            scoreLine.transform.position = scoreLineXTransform;
            

            score = (-1) * scoreLine.transform.position.x;
            if (score < 0)
            {
                scoreText.text = "0";
            }
            else
            {
                scoreText.text = score.ToString();
            }
        }    
    }
    public void SetHightScore()
    {
        stopUpdate = true;

        if (GetFloat("HightScore") > score )
        {
            UIManager.Instance.HightScorePanelControl(false);
            hightScoreLine.transform.position = new Vector3(GetFloat("HightScore") * -1, 0.5f, 8f);
        }
        else
        {
            UIManager.Instance.HightScorePanelControl(true);
            PlayerPrefs.SetFloat("HightScore", score);
            hightScoreLine.transform.position = new Vector3(GetFloat("HightScore") * -1, 0.5f, 8f);
        }
    }
    public void SetLineShow(bool value)
    { 
        if(value == true)
        {
            scoreLine.SetActive(true);
        }
        else
        {
            scoreLine.SetActive(false);
        }
    }
    public void SetFloat(string Score , float value)
    {
        PlayerPrefs.SetFloat(Score, value);
    }
    public float GetFloat(string Score)
    {
        return PlayerPrefs.GetFloat(Score);
    }
}
