using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraMovement : MonoBehaviour
{
    public Transform[] enemys;
    public GameObject player;
    public Vector3 offset;

    public Vector3 mainRotation;

    public static int currentEnemyIndex = 0;

    public float smooth = 5.0f;
    public float smoothSpeed = 1f;

    void LateUpdate()
    {
        if (enemys.Length != currentEnemyIndex)
        {
             if (enemys[currentEnemyIndex].position.x > -5f)
             {
                 transform.DOMove((enemys[currentEnemyIndex].position + offset), 0.5f);
                 transform.rotation = Quaternion.Euler(mainRotation);
             }
             else
             {
                transform.DOMove((enemys[currentEnemyIndex].position + offset + new Vector3(10f, 0f, 0f)) , 2f);
             }
        }
        else
        {
            UIManager.Instance.SetTapToPlay(false);
            transform.rotation = Quaternion.Euler(mainRotation);
            transform.position = player.transform.position + offset;   
        }
    }
    public GameObject GetEnemy()
    {
        //if (currentEnemyIndex < 0) return null;
        return enemys[currentEnemyIndex].gameObject;      
    }
    public void SetCamera()
    {
        StartCoroutine(SetRotateTime());
    }
    IEnumerator SetRotateTime()
    {
        yield return new WaitForSeconds(1.5f);
        transform.DORotate(new Vector3(0, -65f, 0), 1f);
    }
}
