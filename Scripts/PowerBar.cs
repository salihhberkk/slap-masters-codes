using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerBar : MonoBehaviour
{
    CameraMovement camera;
    CharacterMovement character;

    public Image powerBarMask;

    public float barChangeSpeed = 1;
    float maxPowerBarValue = 100;
    float currentPowerBarValue;
    float fill;
    
    [HideInInspector]
    public bool powerBarOn;
    bool slapActive = false;
    bool powerIsIncreasing;

    private void Start()
    {
        camera = FindObjectOfType<CameraMovement>();
        character = FindObjectOfType<CharacterMovement>();

        currentPowerBarValue = 0;
        powerIsIncreasing = false;
        powerBarOn = true;
        PlayCoroutine();
    }
    public void PlayCoroutine()
    {
        StartCoroutine(UpdatePowerBar());
    }
    IEnumerator UpdatePowerBar()
    {
        yield return new WaitForSeconds(1f);
        while (powerBarOn)
        {
            if (powerIsIncreasing)
            {
                currentPowerBarValue += barChangeSpeed;
            }
            else
            {
                currentPowerBarValue -= barChangeSpeed;
            }
           
            fill = currentPowerBarValue / maxPowerBarValue;
            powerBarMask.fillAmount = fill;

            if(fill <= 0f)
            {
                powerIsIncreasing = true;
            }
            else if(fill >= 1f)
            {
                powerIsIncreasing = false;
            }

            yield return new WaitForSeconds(0.02f);

            if (Input.GetMouseButton(0))
            {
                powerBarOn = false;
                //SLAP!!!!!!!!!
                slapActive = true;
                if (slapActive)
                {
                    StartCoroutine(Wait());
                    SetSlapActive();
                }
            }
        }
        yield return null;
    }
    public void SetSlapActive()
    {
        if(fill >= 0.2f)
        {
            GameManager.Instance.Slap(fill);
            slapActive = false;
        }
        else
        {
            camera.GetEnemy().GetComponent<Enemy>().EnemyKickAnim();
            StartCoroutine(WaitKick());     
        }           
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2f);
    }
    IEnumerator WaitKick()
    {
        yield return new WaitForSeconds(1f);
        character.Fall();
    }
}
