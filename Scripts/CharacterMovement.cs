using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterMovement : MonoBehaviour
{
    CameraMovement camera;

    PowerBar powerBar;

    private Transform target;
    private int wavepointIndex = 0;
    public float transitionTime;
    public float animationTime;

    Animator animator;

    Score score;
    CharacterState characterState;
    enum CharacterState
    {
        Idle = 0,
        Kick = 1,
        Jump =2
    }
    void Start()
    {
        camera = FindObjectOfType<CameraMovement>();
        powerBar = FindObjectOfType<PowerBar>();
        characterState = CharacterState.Idle;
        animator = GetComponent<Animator>();

        score = FindObjectOfType<Score>();
        target = Waypoints.staticPoints[0];
    }
    public void NextPoint()
    {
        GetNextWaypoint();
    }

    void GetNextWaypoint()
    {

        StartCoroutine(PlayAnim());
        transform.DOMove(target.transform.position,transitionTime);

        if (wavepointIndex >= Waypoints.staticPoints.Length - 1)
        {
            EndPath();
            return;
        }
        score.stopUpdate = false;
        wavepointIndex++;
        target = Waypoints.staticPoints[wavepointIndex];
    }
    void EndPath()
    {
        UIManager.Instance.PowerBarUIOff();
        UIManager.Instance.NextLevelPanel();
    }
    public void SetAnimation()
    {
        SetState(CharacterState.Kick);      
    }
    private void SetState(CharacterState state)
    {
        characterState = state;

        animator.SetInteger("statecontrol", (int)state);
    }
    IEnumerator PlayAnim()
    {
        SetState(CharacterState.Jump);
        yield return new WaitForSeconds(animationTime);
        SetState(CharacterState.Idle);

    }
    public void Fall()
    {
        transform.DOMoveX(8f, 1f);
        UIManager.Instance.PowerBarUIOff();

        ComeBack();
    }
    public void ComeBack()
    {
        StartCoroutine(WaitForCome());
        
    }
    IEnumerator WaitForCome()
    {
        yield return new WaitForSeconds(1f);
        transform.DOMoveX(0f, 1f);
        UIManager.Instance.PowerBarUIOn();
        powerBar.powerBarOn = true;
        powerBar.PlayCoroutine();
        camera.GetEnemy().GetComponent<Enemy>().IdleAnim();
    }
}
