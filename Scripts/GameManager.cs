using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;
public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    List<Gold> goldList;


    Transform shootPoint;
    public Button nextButton;
    public Button upgradeButton;

    public float startPower;
    public float upgradePower;
    public float weight;
    public float waitAnimationTime;
    [HideInInspector]
    public int currentGold;
    public int upgradeCost;

    private float power;
    [SerializeField] private float enemySwipeSensitivity;

    CameraMovement camera;
    CharacterMovement character;
    Score score;
    PowerBar powerBar;

    private void Start()
    {      
        camera = FindObjectOfType<CameraMovement>();
        character = FindObjectOfType<CharacterMovement>();
        score = FindObjectOfType<Score>();
        powerBar = FindObjectOfType<PowerBar>();

        currentGold = PlayerPrefs.GetInt("Gold", 0);
        power = PlayerPrefs.GetFloat("Power", startPower);

        //shootPoint = enemy.GetShotPoint();
        shootPoint = camera.GetEnemy().GetComponent<Enemy>().GetShotPoint();
    }
    public void Slap(float fill)
    {
        character.SetAnimation();
        UIManager.Instance.PowerBarUIOff();
        UIManager.Instance.SetTapToPlay(false);
        
        StartCoroutine(Wait(fill));

    }
    public void GoToNextPoint()
    {
        new WaitForSeconds(1f);
        CameraMovement.currentEnemyIndex++;
        UIManager.Instance.WinPanelOff();
        score.SetLineShow(false);

        powerBar.powerBarOn = true;
        UIManager.Instance.PowerBarUIOn();
        powerBar.PlayCoroutine();

        character.NextPoint();
        UIManager.Instance.SetTapToPlay(true);
        UIManager.Instance.SetSwipeToPlay(false);

        for (int i = 0; i < goldList.Count; i++)
        {
            if (goldList[i].gameObject.activeSelf == false)
            {
                goldList[i].OpenGold();
            }
        }
    }
    IEnumerator Wait(float fill)
    {
        float force = fill * power * 100;

        camera.SetCamera();
        yield return new WaitForSeconds(waitAnimationTime);
        camera.GetEnemy().GetComponent<Rigidbody>().AddForce(shootPoint.position.x * force, shootPoint.position.y * weight, 0f);
        UIManager.Instance.SetSwipeToPlay(true);
        LeanTouch.OnFingerUpdate += HandleHead;
        camera.GetEnemy().GetComponent<Enemy>().FallAnim();
        score.SetLineShow(true);
    }
    public void UpgradePower()
    {
        
        if (currentGold >= upgradeCost)
        {
            upgradeButton.interactable = true;
            currentGold -= upgradeCost;
            PlayerPrefs.SetInt("Gold", currentGold);
            UIManager.Instance.SetGoldText();

            power += upgradePower;
            PlayerPrefs.SetFloat("Power", power);
            if (currentGold < upgradeCost)
            {
                //gold not enougt
                upgradeButton.interactable = false;
            }
        }   
    }
    public void SetGold()
    {
        currentGold += 1;
        PlayerPrefs.SetInt("Gold", currentGold);
        UIManager.Instance.SetGoldText();
    }
    private void HandleHead(LeanFinger obj)
    {
        if (Input.GetMouseButton(0) && (UIManager.Instance.winPanel.activeSelf == false))
        {
            if (camera.enemys.Length <= 0) return;

            camera.GetEnemy().transform.position += Vector3.forward * (obj.ScaledDelta.x * enemySwipeSensitivity / 100);
            var clampedZ = Mathf.Clamp(camera.GetEnemy().transform.position.z, 2, 12);
            camera.GetEnemy().transform.position = new Vector3(camera.GetEnemy().transform.position.x, camera.GetEnemy().transform.position.y,
                  clampedZ);
        }

    }
    public void CloseTheHandle()
    {
        LeanTouch.OnFingerUpdate -= HandleHead;
    }

}
