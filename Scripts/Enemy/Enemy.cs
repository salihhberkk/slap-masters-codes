using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Collider mainCollider;
    public Rigidbody mainRig;

    [SerializeField] Transform shootPoint;

    int collisionCount;

    Score score;

    Animator animator;
    EnemyState enemyState;
    CameraMovement camera;
    enum EnemyState
    {
        Idle = 0,
        Fall = 1,
        Kick = 2
    }

    private void Start()
    {
        camera = FindObjectOfType<CameraMovement>();
        score = FindObjectOfType<Score>();
        enemyState = EnemyState.Idle;
        animator = GetComponent<Animator>();
    }
    private void Awake()
    {
        GetRagdollBits();
        RagdollModeOff();
    }
    
    public void RagdollModeOn()
    {
        foreach (Collider col in ragDollColliders)
        {
            col.enabled = true;
        }
        foreach (Rigidbody rigid in limbsRigidbodies)
        {
            rigid.isKinematic = false;
        }
        mainCollider.enabled = false;
        mainRig.isKinematic = true;
    }
    public void RagdollModeOff()
    {
        foreach (Collider col in ragDollColliders)
        {
            col.enabled = false;
        }
        foreach (Rigidbody rigid in limbsRigidbodies)
        {
            rigid.isKinematic = true;
        }
        mainCollider.enabled = true;
        mainRig.isKinematic = false;
    }
    Collider[] ragDollColliders;
    Rigidbody[] limbsRigidbodies;
    public void GetRagdollBits()
    {
        ragDollColliders = mainRig.GetComponentsInChildren<Collider>();
        limbsRigidbodies = mainRig.GetComponentsInChildren<Rigidbody>();
    }
    public Transform GetShotPoint()
    {
        return shootPoint;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Path"))
        {
            collisionCount++;
            if (collisionCount == 2)
            {
                UIManager.Instance.WinPanelOn();
                camera.GetEnemy().GetComponent<Enemy>().RagdollModeOn();
                GameManager.Instance.CloseTheHandle();
                score.SetHightScore();
                animator.enabled = false;
            }
        }
    }
    public void EnemyKickAnim()
    {
        SetState(EnemyState.Kick);

    }
    public void FallAnim()
    {
        SetState(EnemyState.Fall);
    }
    public void IdleAnim()
    {
        SetState(EnemyState.Idle);
    }
    private void SetState(EnemyState state)
    {
        enemyState = state;

        animator.SetInteger("setStage", (int)state);
    }
}
