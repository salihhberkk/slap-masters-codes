using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : Singleton<Gold>
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            GameManager.Instance.SetGold();
            gameObject.SetActive(false);
        }
    }
    public void OpenGold()
    {
        gameObject.SetActive(true);
    }

}
