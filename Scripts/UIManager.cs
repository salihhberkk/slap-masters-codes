using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{   
    [SerializeField]
    GameObject powerBar;
    public GameObject winPanel;
    public GameObject nextLevelPanel;
    public GameObject hightScorePanel;
    public GameObject tapToPlay;
    public GameObject swipeToPlay;

    [SerializeField]
    Text goldText;

    private void Start()
    {
        SetGoldText();
    }
    public void SetGoldText()
    {
        goldText.text = PlayerPrefs.GetInt("Gold").ToString();
    }
    public void SetTapToPlay(bool value)
    {
        if(value == true)
        {
            tapToPlay.SetActive(true);
        }
        else
        {
            tapToPlay.SetActive(false);
        }
    }
    public void SetSwipeToPlay(bool value)
    {
        if(value == true)
        {
            swipeToPlay.SetActive(true);
        }
        else
        {
            swipeToPlay.SetActive(false);
        }
    }
    public void PowerBarUIOn()
    {
        powerBar.SetActive(true);
    }
    public void PowerBarUIOff()
    {
        powerBar.SetActive(false);
    }
    public void WinPanelOn()
    { 
        StartCoroutine(WaitPanelOn(1));      
    }
    public void WinPanelOff()
    {
        winPanel.SetActive(false);
    }
    public void NextLevelPanel()
    {
        StartCoroutine(WaitPanelOn(2));
    }
    public void HightScorePanelControl(bool value)
    {
        if(value == true)
        {
            hightScorePanel.SetActive(true);
        }
        else
        {
            hightScorePanel.SetActive(false);
        }
    }
    IEnumerator WaitPanelOn(int index)
    {
        yield return new WaitForSeconds(1f);
       if(index == 1)
        {
            winPanel.SetActive(true);
            SetSwipeToPlay(false);
        }
       else if(index == 2)
        {
            nextLevelPanel.SetActive(true);
        }
    }
}
